##Práctica 4: DHCP
Damián Rivera González

La práctica fue llevada a cabo en VirtualBox 6.0.4

###Creación de red virtual
Para la creación de la red virtual, nos dirigimos a la sección `Archivo -> Preferencias` (o Ctrl+G).
Y vamos a la sedción `Red`. Y le damos clic en el icono de "+" para agregar una nueva red.

![](imagenes/01dhcp.png) 

Se creará una red y le damos clic derecho y seleccionamos `Editar Red NAT`.

Le asignamos un nombre para reconocer, en la sección de `Red CIDR:`damos el valor 192.168.1.1/24
Para la parte de `Opciones de red:` deseleccionamos Sooprta DHCP y Soporta IPv6. Y le damos clic en Aceptar.

![](imagenes/02dhcp.png)

Y luego dentro de la misma ventana `Preferencias` damos clic en el botón Aceptar, para guardar los cambios.

###Configuración de adaptador de red
Creamos una máquina virtual con `Debian 9` (para este caso se le dio el nombre de `machine_one_debian9`), una vez creada la 
apagamos. Seleccionamos la máquina virtual y damos clic derecho sobre ella y `Configuración` (o Ctrl+S). Y nos dirigimos a la 
sección de `Red`. Dentro de la parte `Adaptador 1`, Habilitamos adaptador de red. En la parte de `Conectado a:` seleccionamos 
`Red NAT`, `Nombre:` le asignamos la red que creamos previamente (para este caso `NatNetwork1`). En la sección de 
`Avanzadas`, para `Modo promiscuo:` damos `Permitir todo`. Y damos clic en Aceptar.

![](imagenes/03dhcp.png)

###Configuración de interfaz de red
Encendemos la máquina (machine_one_debian9) y verificamos el nombre de la interfaz de red con el comando `ip a`

![](imagenes/04dhcp.png)

Para este caso se llama (`enp0s3`). Asignamos una IP de manera estática a la interfaz en el archivo `/etc/network/interfaces` 
Aquí es importante notar que la dirección que se le asignará como `gateway` será `192.168.1.1` a diferencia de vmware que usa 
la `192.168.1.2`

![](imagenes/05dhcp.png)

Luego reiniciamos el servicio de la red con el comando 
```
root@debianOne:/home/machineone# systemctl restart networking
```
Verificamos que la interfaz de red tiene la dirección IP asignada

![](imaganes(06dhcp.png)

Agregar la siguiente línea a archivo `/etc/resolv.conf` (esto permitira hacer búsquedas de `DNS`), recordemos que es 
VirtualBox por lo que la dirección cambia en el útlimo dígito:

```
nameserver 192.168.1.1
```

Verificar que se tiene acceso a internet con `ping`

![](imagenes/07dhcp.png)

### Servidor DHCP

Instalamos el servidor DHCP

```
apt install isc-dhcp-server
```
![](imagenes/08dhcp.png)

Modificar archivo de configuración de servidor `/etc/dhcp/dhcpd.conf`, indicando el rango de direcciones IP a ser 
distribuidas, junto con una puerta de enlace predeterminada y un servidor DNS:

![](imagenes/09dhcp.png)

Desactivar uso de interfaz para IPv6, en archivo `/etc/default/isc-dhcp-server` comentar la siguiente línea:

![](imagenes/10dhcp.png)

Borrar contenido de archivo `/etc/dhcp/dhcpd6.conf` y reiniciar servicio:

```
root@debianOne:/home/machineone# mv /etc/dhcp/dhcpd6.conf /etc/dhcp/dhcpd6.conf.bck
root@@debianOne:/home/machineone# touch /etc/dhcp/dhcpd6.conf
root@@debianOne:/home/machineone# systemctl restart isc-dhcp-server
```

Crear máquina virtual cliente con Debian 9, repetir la **Configuración de adaptador de red** para esta máquina.

Capturar tráfico de red con Wireshark o tcpdump (guardando tráfico en un archivo). El tráfico de red debe de ser únicamente 
el de la red virtual que fue creada, notar que en la máquina física se creó una interfaz de red virtual asociada a ésta.

Prender máquina y verificar que se le asignó una dirección IP en el rango de direcciones IP asignadas por el servidor DHCP.

![](imagenes/11dhcp.png)

En servidor DHCP, observar que en la bitácora del sistema se muestra información sobre la asignación de la dirección IP al 
cliente:

![](imagenes/12dhcp.png)


###PXE

Modificamos el archivo `/etc/dhcp/dhcpd.conf`

![](imagenes/13dhcp.png)

Reiniciamos el servicio de DHCP

```
systemctl restart isc-dhcp-server
```

Instalamos el servidor TFTP

![](imagenes/14dhcp.png)

Descargamos la  imagen de sistema operativo:

```
root@@debianOne:/home/machineone# cd /srv/tftp/
root@@debianOne:/home/machineone# wget http://ftp.mx.debian.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/netboot.tar.gz
root@@debianOne:/home/machineone# tar xzvf netboot.tar.gz
root@@debianOne:/home/machineone# chmod -R a+r *
root@@debianOne:/home/machineone# systemctl restart tftpd-hpa
```

Creamos una nueva máquina virtual. Una vez creada, la seleccionamos y damos clic derecho `Configuración`, nos dirigimos a la 
sección de `Sistema`. En la sección de `Orden de arranque:` deseleccionamos todo y seleccionamos `Red`.

![](imagenes/15dhcp.png)

Luego configuramos la red virtual creada. Dentro de la misma ventana de `Configuración` vamos a la sección de `Red` y 
configuramos: `Conectado a: Red NAT`, `Nombre:` seleccionamos el nombre de la red que creamos desde un inicio (para este caso 
`NatNetwork1`), damos en `Avanzadas` y en la sección de `Modo promiscuo:` damos `Permitir MVs`.

![](imagenes/16dhcp.png)

Y damos clic en Aceptar.

Y encendemos la máquina. Cuando nos pude la imagen para arrancar la máquina dejamos el valor "vacío" y de damos en cancelar.

Vemos de un lado el tráfico y del otro que si ha cargado la imagen en la máquina virtual desde la red.

![](imagenes/17dhcp.png)

Mostramos la captura del tráfico, donde se muestra el funcionamiento de DHCP y la descarga de archivos por el protocolo TFTP.

![](imagenes/18dhcp.png)


### Preguntas
¿Cómo funciona el protocolo DHCP? Medinate cuatro mensajes; el cliente manda un mensaje (DHCP DISCOVERY) en broadcast, para 
que algun servidor DHCP le responda, algún servidor le manda un mensaje de respuesta (DHCP OFFER) que le ofrece sus servicios 
el cliente elige algún servidor específico y le manda un mensaje (DHCP REQUEST) solicitando sus servicios, y al final el 
servidor le manda un mensaje para asignarle su IP (DHCP ACKNOWLEDGE)

¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?

(Windows)
Abrimos Wireshark
Abrimos una terminal 
Ingresamos ipconfig/renew (Enter)
Ingresamos ipconfig/release (Enter)
Ingresamos ipconfig/renew (Enter)
Y paramos la captura de Wireshark

(Linux)
WIRESHARK: Filtramos por: udp.port == 67 or udp.port == 68
TCPDUMP : tcpdump -i <nombre-interfaz> -n port 67 and port 68

¿Qué es el estándar PXE y cuál es su utilidad? Es un entorno de ejecución que se lleva antes del arranque dentro de la 
tarjeta de red. Se utiliza para hacer poder hacer configuracióndes de inicio en una máquina sin necesidad de un SO 
precargado.
