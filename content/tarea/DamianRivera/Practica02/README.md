************

## Práctica 2: Sockets
Damián Rivera González

***********
### Contenido.
(Cabe aclarar que las bases del código fueron tomadas del enlace "https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_C/Sockets", solo se hicieron las modificaciones necesarias para cumplir con las especificaciones.)

+ Cliente.c : un archivo con el código fuente que funciona como un cliente que se conecta al servidor.
	
+ Servidor.c : un archivo von el código que funciona como un servidor, este puede escuchar y esperar conexines desde los clientes.

	
#### Funcionamiento.

Una vez que se haga uso del comando "make", se crearan dos archivos ejecutables
"Servidor" y "Cliente", los cuales se usarán para la conexión.

Desde una terminal se deberá ejecutar el programa Servidor, siempre primero que el Cliente con la siguiente instrucción 

```	
./Servidor <puerto>  donde el puerto debe ser un dígito. ejemplo
./Servidor 54321
```
Luego de ello desde otra terminal ejecutar el archivo "Cliente", con la siguiente instrucción.

```
./Cliente <IP> <Puerto> donde la IP puede ser cualquier IP (se recomienda 0.0.0.0) y el mismo puerto con el que se creo el 
Servidor. 
ejemplo: ./Cliente 0.0.0.0 54321
```

![](media/sercli.png)
