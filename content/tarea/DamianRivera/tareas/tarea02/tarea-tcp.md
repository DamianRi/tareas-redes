## Tarea TCP

### 1. Verificar que el dominio tenga el servicio de HTTP

Revisar que el nombre `www` para el dominio seleccionado responda al protocolo HTTP por el puerto `80/tcp`:

![](media/verificacion.png)

### 2. Captura de tráfico

Es necesario realizar una captura del tráfico de red que se genera para la comunicación entre el cliente y el servidor, para esto se puede utilizar cualquiera de las siguientes herramientas:

#### B. Utilizando `wireshark`

##### Configuración para `wireshark`

Configuramos `wireshark` agregando el usuario actual (para este caso es `damri`

![](media/adduser.png)

##### Captura de tráfico

Abrir `wireshark` en una terminal:

```
$ wireshark &
```

Hacemos una captura de tráfico únicamente para el dominio `www.ligabancomer.mx`

![](media/wireshark.png)

### 3. Petición HTTP

Realizamos una petición HTTP utilizando `curl`, donde:

+ La opción `-o` guarda la salida en el archivo `www_ligabancomer_mx.http.html`
+ `tr -d` borra todas las ocurrencias del caracter `\r` (CR)
+ `tee` imprime la salida y al mismo tiempo la guarda en el archivo `www_ligabancomer_mx.http.log`

```
$ curl -v# 'http://www.ligabancomer.mx/' -o www_ligabancomer_mx.http.html 2>&1 | tr -d '\r' | tee www_ligabancomer_mx.http.log
*   Trying 189.204.118.73...
* TCP_NODELAY set
* Connected to www.ligabancomer.mx (189.204.118.73) port 80 (#0)
> GET / HTTP/1.1
> Host: www.ligabancomer.mx
> User-Agent: curl/7.58.0
> Accept: */*
> 
#=O#- #                                                                                                 < HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: text/html; charset=utf-8
< ETag: W/"6732e-7DfIW246FK9RoQNUd1FUBw"
< Vary: Accept-Encoding
< Date: Sun, 05 May 2019 18:26:49 GMT
< Connection: keep-alive
< Set-Cookie: f5avrbbbbbbbbbbbbbbbb=ACOLAENLAGPDJOHNGMDPCMHKBALLPHEBFIMOKACJKLIOAPBKEBJAHKLBENFFHHCMLEODBNCBBKLBIAFKCAJAFCNGHCNHPHIADGENHGHBBPABOBCJHOGOIBANPFDGNFDN; HttpOnly
< Set-Cookie: f5_cspm=1234;
< Transfer-Encoding: chunked
< 
{ [2629 bytes data]
##O=# # #-#O=# # #-#O=-# # #-#O=-# # #-=#=- # # #-=O#- # # #-=O#- # # #=O=# # # #=O=-# # # -#O=- 
### # # -=#=- # # # -=O=# # # # -=O=-# # # # -=O=- # # # # -=O=- # # # # -=O=- # # # # * 
##Connection #0 to host www.ligabancomer.mx left intact
```

![](media/captura.png)

### 4. Cerrar la captura de tráfico y visualizar el resultado

#### B. Utilizando `wireshark`

Detener la captura de tráfico.
Y la guardamos en el archivo `www_ligabancomer_mx.pcap`

#### Guardar la versión en texto de la captura `pcap`

Guardar la versión en texto de la captura realizada en el archivo `www_ligabancomer_mx.pcap.txt`
```
#tcpdump -#envr www_ligabancomer_mx.pcap 2>&1 | tee www_ligabancomer_mx.pcap.txt
reading from file www_ligabancomer_mx.pcap, link-type LINUX_SLL (Linux cooked)
    1  15:31:39.781418 Out 98:28:a6:0f:e9:a1 ethertype IPv4 (0x0800), length 76: (tos 0x0, ttl 64, id 11366, offset 0, flags [DF], proto TCP (6), length 60)
    192.168.100.31.38040 > 189.204.118.73.80: Flags [S], cksum 0x69e4 (correct), seq 1252188216, win 29200, options [mss 1460,sackOK,TS val 2743250919 ecr 0,nop,wscale 7], length 0
    2  15:31:39.846395  In 58:f9:87:62:bb:c1 ethertype IPv4 (0x0800), length 76: (tos 0x0, ttl 242, id 22453, offset 0, flags [DF], proto TCP (6), length 60)
    189.204.118.73.80 > 192.168.100.31.38040: Flags [S.], cksum 0xab20 (correct), seq 8502427, ack 1252188217, win 4236, options [mss 1412,nop,nop,TS val 2310134945 ecr 2743250919,sackOK,eol], length 0
    3  15:31:39.846475 Out 98:28:a6:0f:e9:a1 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 11367, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.100.31.38040 > 189.204.118.73.80: Flags [.], cksum 0x74ee (correct), ack 1, win 29200, options [nop,nop,TS val 2743250984 ecr 2310134945], length 0
    4  15:31:39.846685 Out 98:28:a6:0f:e9:a1 ethertype IPv4 (0x0800), length 151: (tos 0x0, ttl 64, id 11368, offset 0, flags [DF], proto TCP (6), length 135)
    192.168.100.31.38040 > 189.204.118.73.80: Flags [P.], cksum 0x3224 (correct), seq 1:84, ack 1, win 29200, options [nop,nop,TS val 2743250984 ecr 2310134945], length 83: HTTP, length: 83
	GET / HTTP/1.1
	Host: www.ligabancomer.mx
	User-Agent: curl/7.58.0
	Accept: */*
```

#### Diagrama de la captura realizada

![](media/http.png)


