#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>


/**
 * Función para obtener el valor hash md5 de un archivo.
 * @param  *archivo: el archivo que se obtendrá el valor md5
 */
void md5(char *archivo)
{
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    FILE *inFile = fopen (archivo, "rb");
    MD5_CTX mdContext;
    int bytes;
    unsigned char data[1024];

    if (inFile == NULL) {
        printf ("%s no se puede abrir.\n", archivo);
    }

    MD5_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) printf("%02x", c[i]);
    printf (" %s\n", archivo);
    fclose (inFile);
}


/**
 * Función para obtener el valor hash sha1 de un archivo.
 * @param  *archivo: el archivo que se obtendrá el valor sha1
 */
void sha1(char *archivo)
{
    unsigned char c[SHA_DIGEST_LENGTH];
    int i;
    FILE *inFile = fopen (archivo, "rb");
    SHA_CTX mdContext;
    int bytes;
    unsigned char data[1024];

    if (inFile == NULL) {
        printf ("%s no se puede abrir.\n", archivo);
    }

    SHA1_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        SHA1_Update (&mdContext, data, bytes);
    SHA1_Final (c,&mdContext);
    for(i = 0; i < SHA_DIGEST_LENGTH; i++) printf("%02x", c[i]);
    printf (" %s\n", archivo);
    fclose (inFile);
}

/**
 * Función para obtener el valor hash sha256 de un archivo.
 * @param  *archivo: el archivo que se obtendrá el valor sha256
 */
void sha256(char *archivo)
{
    unsigned char c[SHA256_DIGEST_LENGTH];
    int i;
    FILE *inFile = fopen (archivo, "rb");
    SHA256_CTX mdContext;
    int bytes;
    unsigned char data[1024];

    if (inFile == NULL) {
        printf ("%s no se puede abrir.\n", archivo);
    }

    SHA256_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        SHA256_Update (&mdContext, data, bytes);
    SHA256_Final (c,&mdContext);
    for(i = 0; i < SHA256_DIGEST_LENGTH; i++) printf("%02x", c[i]);
    printf (" %s\n", archivo);
    fclose (inFile);
}

int main(int argc, char **argv) {
	if (argc >= 3){
        //iteramos sobre todos los paramteros recibidos.
        for(int i = 2; i < argc; i++)
        {
            //aplicamos md5 a cada archivo.
            if (!strcmp(argv[1], "md5")) {
                md5(argv[i]);
            //aplicamos sha1 a cada archivo
            }else if(!strcmp(argv[1], "sha1")){
                sha1(argv[i]);
            //aplicamos sha256 a cada archivo
            }else if(!strcmp(argv[1], "sha256")){
                sha256(argv[i]);
            }else{
                printf("Segundo argumento <opcion> no reconocido\n<opcion> := md5 o sha1 o sha256\n");
                return 0;
            }
        }
    }else
		printf("No hay suficientes parametros\n Uso: ./hash <opcion> <archivos> ...\n <opcion>:= md5 o sha1 o sha256\n");
	return 0;
}
